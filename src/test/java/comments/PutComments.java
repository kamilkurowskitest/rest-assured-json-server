package comments;

import io.restassured.http.ContentType;
import org.junit.jupiter.api.Test;

import java.util.HashMap;
import java.util.Map;

import static io.restassured.RestAssured.given;

public class PutComments {

    final String COMMENTS_URL = "http://localhost:3000/comments/555";

    @Test
    void shouldReturnCode201WhenCommentIsUpdated() {
        Map<String, Object> updateTitle = new HashMap<>();
        updateTitle.put("title", "Kamil-test-123");

        given().contentType(ContentType.JSON).
                and().body(updateTitle).when().
                put(COMMENTS_URL).then().
                statusCode(200);

    }
}
